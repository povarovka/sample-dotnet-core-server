var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app.MapGet("/", () => "Hello World! This is a .NET Core HTTP server");

app.Run();

// http://localhost:5077/